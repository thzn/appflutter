import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'Agendamento.dart';
import 'package:flutter_boom_menu/flutter_boom_menu.dart';

class FancyFab extends StatefulWidget {
//  final Function() onPressed;
//  final String tooltip;
//  final IconData icon;
//  FancyFab({this.onPressed, this.tooltip, this.icon});
  @override
  _FancyFabState createState() => _FancyFabState();
}
class _FancyFabState extends State<FancyFab>
    with SingleTickerProviderStateMixin {
//  bool isOpened = false;
//  AnimationController _animationController;
//  Animation<Color> _buttonColor;
//  Animation<double> _animateIcon;
//  Animation<double> _translateButton;
//  Curve _curve = Curves.easeOut;
//  double _fabHeight = 56.0;
  ScrollController scrollController;
  bool scrollVisible = true;
  @override
  initState() {
//    _animationController =
//        AnimationController(vsync: this, duration: Duration(milliseconds: 500))
//          ..addListener(() {
//            setState(() {});
//          });
//    _animateIcon =
//        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
//    _buttonColor = ColorTween(
//      begin: Colors.blue,
//      end: Colors.red,
//    ).animate(CurvedAnimation(
//      parent: _animationController,
//      curve: Interval(
//        0.00,
//        1.00,
//        curve: Curves.linear,
//      ),
//    ));
//    _translateButton = Tween<double>(
//      begin: _fabHeight,
//      end: -14.0,
//    ).animate(CurvedAnimation(
//      parent: _animationController,
//      curve: Interval(
//        0.0,
//        0.75,
//        curve: _curve,
//      ),
//    ));
    super.initState();
    scrollController = ScrollController()
      ..addListener(() {
        setDialVisible(scrollController.position.userScrollDirection ==
            ScrollDirection.forward);
      });
  }
//  @override
//  dispose() {
//    _animationController.dispose();
//    super.dispose();
//  }
  @override
  setDialVisible(bool value) {
    setState(() {
      scrollVisible = value;
    });
  }
//  animate() {
//    if (!isOpened) {
//      _animationController.forward();
//    } else {
//      _animationController.reverse();
//    }
//    isOpened = !isOpened;
//  }

//  Widget image() {
//    return Container(
//      child: FloatingActionButton(
//        heroTag: "image",
//        onPressed: () {
//          Navigator.push(
//            context,
//            MaterialPageRoute(builder: (context) => Agendamento()),
//          );
//        },
//        tooltip: 'Image',
//        child: Icon(Icons.add),
//      ),
//    );
//  }

//  Widget inbox() {
//    return Container(
//      child: FloatingActionButton(
//        heroTag: "inbox",
//        backgroundColor: Colors.yellowAccent,
//        onPressed: () {
//          Navigator.push(
//            context,
//            MaterialPageRoute(builder: (context) => Agendamento()),
//          );
//        },
//        tooltip: 'Natural',
//        child: Icon(Icons.add),
//      ),
//    );
//  }
//
//  Widget toggle() {
//    return Container(
//      child: FloatingActionButton(
//        heroTag: "toggle",
//        backgroundColor: _buttonColor.value,
//        onPressed: animate,
//        tooltip: 'Toggle',
//        child: AnimatedIcon(
//          icon: AnimatedIcons.add_event,
//          progress: _animateIcon,
//        ),
//      ),
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Column(
//      mainAxisAlignment: MainAxisAlignment.end,
//      children: <Widget>[
//        Transform(
//          transform: Matrix4.translationValues(
//            0.0,
//            _translateButton.value * 2.0,
//            0.0,
//          ),
//          child: image(),
//        ),
//        Transform(
//          transform: Matrix4.translationValues(
//            0.0,
//            _translateButton.value,
//            0.0,
//          ),
//          child: inbox(),
//        ),
//        toggle(),
//      ],
//    );
//  }
  @override
  Widget build(BuildContext context) {
    return BoomMenu(
        animatedIcon: AnimatedIcons.menu_close,
        animatedIconTheme: IconThemeData(size: 22.0),
        //child: Icon(Icons.add),
        onOpen: () => print('OPENING DIAL'),
        onClose: () => print('DIAL CLOSED'),
        scrollVisible: scrollVisible,
        overlayColor: Colors.black,
        overlayOpacity: 0.7,
        children: [
          MenuItem(
//            child: Image.asset('assets/profile_icon.png', color: Colors.white),
            title: "Perfil",
            titleColor: Colors.white,
            subtitle: "Lorem ipsum dolor sit amet, consectetur",
            subTitleColor: Colors.white,
            backgroundColor: Colors.blue,
            onTap: () => print('FOURTH CHILD'),
          ),
          MenuItem(
//            child: Image.asset('assets/schemes_icon.png', color: Colors.white),
            title: "Agendar Bronzeamento",
            titleColor: Colors.white,
            subtitle: "Natural ou Artificial",
            subTitleColor: Colors.white,
            backgroundColor: Colors.pinkAccent,
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Agendamento()),
            ),
          ),
          MenuItem(
//          child: Icon(Icons.accessibility, color: Colors.black, size: 40,),
//            child: Image.asset('assets/logout_icon.png', color: Colors.grey[850]),
            title: "Sair",
            titleColor: Colors.grey[850],
            subtitle: "Lorem ipsum dolor sit amet, consectetur",
            subTitleColor: Colors.grey[850],
            backgroundColor: Colors.grey[50],
            onTap: () => print('THIRD CHILD'),
          )
        ]);
  }
}